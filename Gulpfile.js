var gulp = require('gulp'),
    connect = require('gulp-connect'),
    historyApiFallback = require('connect-history-api-fallback'),
    postcss = require('gulp-postcss'),
    precss = require('precss'),
    mqpacker = require('css-mqpacker'),
    cssnano = require('gulp-cssnano'),
    pxtorem = require('postcss-pxtorem'),
    cssnext = require('postcss-cssnext'),
    inject = require('gulp-inject'),
    bem = require('postcss-bem'),
    wiredep = require('wiredep').stream;

// Servidor web de desarrollo
gulp.task('server', function() {
  connect.server({
    root: './app',
    hostname: '0.0.0.0',
    port: 8080,
    livereload: true,
    middleware: function(connect, opt) {
      return [ historyApiFallback() ];
    }
  })
});
// Vigila cambios que se produzcan en el código y lanza las tareas relacionadas
gulp.task('watch', function() {
    gulp.watch(['./app/**/*.html'],['html']);
    gulp.watch(['./app/post/**/*.css'], ['css']);
    gulp.watch(['./app/css/**/*.css'], ['css','inject']);
    gulp.watch(['./app/js/**/*.js'], ['inject']);
    gulp.watch(['./bower.json'], ['wiredep']);
});
// Recarga el navegador cuando hay cambios en el HTML
gulp.task('html', function() {
   gulp.src('./app/**/*.html')
   .pipe(connect.reload());
});
//manejador de postcss
gulp.task('css', function () {
  var processors = [
    precss, //minipreprosesador css3 parecido a Sass
    cssnext,
    mqpacker, //envia el mediaqueries al final
    bem({style: 'bem'}),
    pxtorem({
            replace: false//redondea pixel a rem y la opcion false es para evitar que remplace la medida real para dejarlo como fallback
        })
  ];
  return gulp.src('./app/post/*.css')
    .pipe(postcss(processors))
    //.pipe(cssnano())//optimizador de css
    .pipe(gulp.dest('./app/css'))
    .pipe(connect.reload());
});
// Busca estilos y javascript nuevos para inyectarlos en el index.html
gulp.task('inject', function () {
  var target = gulp.src('index.html', {cwd: './app'});
  var sources = gulp.src(['./app/js/**/*.js','./app/css/**/*.css'], {read: false});
  return target.pipe(inject(sources, {
    ignorePath: '/app'
  }))
    .pipe(gulp.dest('./app'));
});
// Inyecta las librerias que instalemos vía Bower
gulp.task('wiredep', function () {
  gulp.src('./app/index.html')
  .pipe(wiredep({
    directory: './app/lib'
  }))
  .pipe(gulp.dest('./app'));
});

gulp.task('default', ['server','inject', 'wiredep','css','watch']);
